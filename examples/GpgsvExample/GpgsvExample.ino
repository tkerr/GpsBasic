/******************************************************************************
 * GpsBasicExample.ino
 * Copyright (c) 2022 Thomas Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief Example sketch for the GpsBasic class.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <Arduino.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "GpsGpgsv.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/
void print_gpgsv();


/******************************************************************************
 * Local definitions.
 ******************************************************************************/
#define MONITOR_SERIAL_BAUD (115200)  //!< Arduino serial monitor baud rate
#define GPS_SERIAL_BAUD (9600)        //!< GPS module baud rate


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

// The GPGSV parser object, subclassed from GpsBasic.
GpsGpgsv my_gpgsv;

// Global ISerial object for the GPS interface.
// This is the serial port that is connected to the GPS.
ISerial gps_serial;

// Global serial monitor object.
// The is the serial port that prints messages to the user.
ISerial monitor;

 
/******************************************************************************
 * Local data.
 ******************************************************************************/


/******************************************************************************
 * Public functions.
 ******************************************************************************/


/**************************************
 * setup()
 **************************************/ 
void setup()
{
    // Turn the Arduino built-in LED off.
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
    
    // Arduino serial monitor init.
    monitor.setSerial(&Serial);
    monitor.begin(MONITOR_SERIAL_BAUD);
    delay(2000);
    monitor.println("GpsBasic: $GPGSV parser subclass example sketch.");
    
    // GPS serial interface init.
    // Use the serial port connected to the GPS module.
    gps_serial.setSerial(&Serial1);
    if (!gps_serial.begin(GPS_SERIAL_BAUD))
    {
        monitor.println("GPS serial port init FAILED.");
    }
    
    // GPS controller init.
    my_gpgsv.setGpsSerial(&gps_serial);
    if (!my_gpgsv.begin())
    {
        monitor.println("GPS controller init FAILED.");
    }
}

/**************************************
 * loop()
 **************************************/ 
void loop()
{
    static char nmea_raw[256];
    
    // Run the periodic service routine.
    GpsBasic::NmeaType nmea = my_gpgsv.service();
    
    if (nmea == GpsBasic::NMEA_GPGSV)
    {
        my_gpgsv.copyNmea(nmea_raw);  // Copy and print the GPGSV sentence
        monitor.println(nmea_raw);
        print_gpgsv();                // Print the parsed GPGSV sentence
            
        if (my_gpgsv.hasFix())
        {
            // We have a fix - turn LED on.
            digitalWrite(LED_BUILTIN, HIGH);  
        }
        else
        {
            // No fix. turn LED off.
            digitalWrite(LED_BUILTIN, LOW);
        }
    }
}


/**************************************
 * print_gpgsv()
 **************************************/ 
void print_gpgsv()
{
    GpgsvData data = my_gpgsv.gpgsv();
    
    // Wait until the last GPGSV message is received.
    if ((data.num_msgs > 0) && (data.last_msg == data.num_msgs))
    { 
        monitor.print("Sats in view: "); monitor.println(data.sats_in_view);
        monitor.print("Num msgs: "); monitor.println(data.num_msgs);
        for (int i = 0; i < GpgsvData::MAX_SATS; i++)
        {
            monitor.print(i+1); 
            monitor.print(": PRN: "); monitor.print(data.sat[i].prn);
            monitor.print(" EL: "); monitor.print(data.sat[i].el_deg);
            monitor.print(" AZ: "); monitor.print(data.sat[i].az_deg);
            monitor.print(" SNR: "); monitor.print(data.sat[i].snr);
            monitor.println();
        }
    }
}


/******************************************************************************
 * Private functions.
 ******************************************************************************/

// End of file.