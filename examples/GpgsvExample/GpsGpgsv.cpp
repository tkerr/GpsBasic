/******************************************************************************
 * GpsGpgsv.cpp
 * Copyright (c) 2022 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Arduino GPS controller subclass used for parsing $GPGSV NMEA sentences.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include "Arduino.h"


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "GpsGpgsv.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/


/******************************************************************************
 * Local data.
 ******************************************************************************/


/******************************************************************************
 * Public methods and functions.
 ******************************************************************************/

/**************************************
 * GpsGpgsv::GpsGpgsv()
 **************************************/
GpsGpgsv::GpsGpgsv()
{
    GpsBasic();
    m_gpgsv.clear();
}


/******************************************************************************
 * Protected methods and functions.
 ******************************************************************************/

/**************************************
 * GpsGpgsv::parseGpgsv()
 **************************************/
void GpsGpgsv::parseGpgsv()
{
    static const int MSG_MAX_SATS = 4;              // Maximum satellites per message
    char* msgCntStr = nmeaStrtok(&m_gps_rxbuf[7]);  // Message count
    char* msgNumStr = nmeaStrtok(NULL);             // Message number
    char* svCntStr  = nmeaStrtok(NULL);             // Number of satellites in view
    
    int msg_num  = atoi(msgNumStr);
    int sat_idx = (msg_num > 0) ? (msg_num - 1) * MSG_MAX_SATS : 0;
    
    m_gpgsv.num_msgs = atoi(msgCntStr);
    m_gpgsv.sats_in_view = atoi(svCntStr);
    m_gpgsv.last_msg = msg_num;
    
    if (sat_idx > (GpgsvData::MAX_SATS - MSG_MAX_SATS)) return;  // Too many satellites

    for (int i = 0; i < MSG_MAX_SATS; i++)
    {
        char* prnStr = nmeaStrtok(NULL);
        char* elStr  = nmeaStrtok(NULL);
        char* azStr  = nmeaStrtok(NULL);
        char* snrStr = nmeaStrtok(NULL);
        
        // Some strings may be empty.
        m_gpgsv.sat[sat_idx + i].prn = (*prnStr) ? atoi(prnStr) : 0;
        m_gpgsv.sat[sat_idx + i].el_deg = (*elStr) ? atoi(elStr) : -1;
        m_gpgsv.sat[sat_idx + i].az_deg = (*azStr) ? atoi(azStr) : -1;
        m_gpgsv.sat[sat_idx + i].snr = (*snrStr) ? atoi(snrStr) : -1;
    }
}


/*****************************************************************************
 * Private methods and functions.
 ******************************************************************************/


// End of file.
