/*****************************************************************************
 * GpsGpgsv.h
 * Copyright (c) 2022 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ****************************************************************************/

/**
 * @file
 * @brief
 * Arduino GPS controller subclass used for parsing $GPGSV NMEA sentences.
 */
#ifndef _GPS_GPGSV_H_
#define _GPS_GPGSV_H_

/******************************************************************************
 * System include files.
 ******************************************************************************/


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "GpsBasic.h"


/******************************************************************************
 * Public definitions.
 ******************************************************************************/
 
 
/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

 
/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/

/**
 * @class GpsGpgsv
 * @brief Example GpsBasic subclass.
 *
 * Class used to parse and print $GPGSV NMEA sentences.
 * Provided as an example subclass of the GpsBasic base class.
 */
class GpsGpgsv : public GpsBasic
{
public:

    GpsGpgsv(); //!< Default constructor
    
    inline const GpgsvData& gpgsv() {return m_gpgsv;}  //!< Return the parsed data
    
protected:

    virtual void parseGpgsv();  //!< $GPGSV parser method, overrides base method
    
    GpgsvData m_gpgsv;          //!< GPGSV data container
};


#endif // _GPS_GPGSV_H_
