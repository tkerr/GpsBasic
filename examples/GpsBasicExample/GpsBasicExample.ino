/******************************************************************************
 * GpsBasicExample.ino
 * Copyright (c) 2022 Thomas Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief Example sketch for the GpsBasic class.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <Arduino.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "GpsBasic.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/
void print_data();


/******************************************************************************
 * Local definitions.
 ******************************************************************************/
#define MONITOR_SERIAL_BAUD (115200)  //!< Arduino serial monitor baud rate
#define GPS_SERIAL_BAUD (9600)        //!< GPS module baud rate


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

// The basic GPS controller object.
GpsBasic my_gps;

// Global ISerial object for the GPS interface.
// This is the serial port that is connected to the GPS.
ISerial gps_serial;

// Global serial monitor object.
// The is the serial port that prints messages to the user.
ISerial monitor;

 
/******************************************************************************
 * Local data.
 ******************************************************************************/


/******************************************************************************
 * Public functions.
 ******************************************************************************/


/**************************************
 * setup()
 **************************************/ 
void setup()
{
    // Turn the Arduino built-in LED off.
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
    
    // Arduino serial monitor init.
    monitor.setSerial(&Serial);
    monitor.begin(MONITOR_SERIAL_BAUD);
    delay(2000);
    monitor.println("GpsBasic example sketch.");
    
    // GPS serial interface init.
    // Use the serial port connected to the GPS module.
    gps_serial.setSerial(&Serial1);
    if (!gps_serial.begin(GPS_SERIAL_BAUD))
    {
        monitor.println("GPS serial port init FAILED.");
    }
    
    // GPS controller init.
    my_gps.setGpsSerial(&gps_serial);
    if (!my_gps.begin())
    {
        monitor.println("GPS controller init FAILED.");
    }
}

/**************************************
 * loop()
 **************************************/ 
void loop()
{
    static char nmea_raw[256];
    
    // Run the periodic service routine.
    GpsBasic::NmeaType nmea = my_gps.service();
    
    if (nmea == GpsBasic::NMEA_GPRMC)
    {
        my_gps.copyNmea(nmea_raw);  // Copy and print the GPRMC sentence
        monitor.println(nmea_raw);
            
        // Print portions of the parsed data structure when a GPRMC sentence
        // is received and we have a fix.
        if (my_gps.hasFix())
        {
            digitalWrite(LED_BUILTIN, HIGH);  // We have a fix - turn LED on
            print_data();                     // Print the formatted data structure
            monitor.println();
        }
        else
        {
            // No fix. turn LED off.
            digitalWrite(LED_BUILTIN, LOW);
        }
    }
    else if (nmea != GpsBasic::NMEA_NONE)
    {
        // Print the raw (un-parsed) NMEA sentence.
        my_gps.copyNmea(nmea_raw);
        monitor.println(nmea_raw);
    }
}

/**************************************
 * print_data()
 **************************************/
void print_data()
{
    GpsData data = my_gps.data();
    
    monitor.print("Date: "); 
    monitor.print(data.year);
    monitor.print('-');
    monitor.print(data.month);
    monitor.print('-');
    monitor.print(data.day);
    monitor.print(' ');
    monitor.print(data.hour);
    monitor.print(':');
    monitor.print(data.minute);
    monitor.print(':');
    monitor.print(data.second);
    monitor.print(" (");
    monitor.print(data.date_r);
    monitor.print(' ');
    monitor.print(data.time_r, 3);
    monitor.println(')');
    
    monitor.print("Lat: "); monitor.print(data.lat_d, 6);
    monitor.print(" ("); monitor.print(data.lat_r, 4); monitor.print(data.lat_h); monitor.println(')');
    monitor.print("Lon: "); monitor.print(data.lon_d, 6);
    monitor.print(" ("); monitor.print(data.lon_r, 4); monitor.print(data.lon_h); monitor.println(')');
    monitor.print("Alt: "); monitor.print(data.alt, 1); monitor.println(data.alt_m);
    
    monitor.print("Speed: "); monitor.print(data.speed_k, 2);
    monitor.print(" km/h Course: "); monitor.print(data.tcourse, 2); monitor.println(" deg true");
    
    monitor.print("Sats: "); monitor.println(data.numsats);
    monitor.print("Mode: "); monitor.print(data.mode1); monitor.println(data.mode2);
    monitor.print("PDOP: "); monitor.print(data.pdop, 2);
    monitor.print(" HDOP: "); monitor.print(data.hdop, 2);
    monitor.print(" VDOP: "); monitor.println(data.vdop, 2);
}

/******************************************************************************
 * Private functions.
 ******************************************************************************/

// End of file.