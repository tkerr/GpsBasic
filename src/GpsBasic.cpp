/******************************************************************************
 * GpsBasic.cpp
 * Copyright (c) 2022 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Arduino basic GPS controller class.
 *
 * Provides basic hardware-independent GPS functionality for receiving and 
 * parsing NMEA messages.  Can be subclassed to support specific hardware 
 * modules.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "GpsBasic.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/
static uint16_t asc2dec(char* asc);  //!< Convert two ASCII digits to a number
static uint16_t asc3dec(char* asc);  //!< Convert three ASCII digits to a number
static uint8_t  hex2bin(char hex);   //!< Convert an ASCII hex digit to a number


/******************************************************************************
 * Local definitions.
 ******************************************************************************/


/******************************************************************************
 * Local data.
 ******************************************************************************/

// Dummy strtok() pointer used to avoid 'unused variable' compiler warnings
static char* dummy_ptr = NULL;


/******************************************************************************
 * Public methods and functions.
 ******************************************************************************/

/**************************************
 * GpsBasic::GpsBasic
 **************************************/
GpsBasic::GpsBasic() :
    m_pStrtok(0),
    m_index(0),
    m_pGpsSerial(0)
{
    m_data.clear();
    memset(m_gps_rxbuf, 0, GPS_SERIAL_BUFSIZE);
    memset(m_nmea_buf, 0, GPS_SERIAL_BUFSIZE);
}

/**************************************
 * GpsBasic::begin
 **************************************/
bool GpsBasic::begin()
{
    m_pStrtok = 0;
    m_index = 0;
    m_data.clear();
    memset(m_gps_rxbuf, 0, GPS_SERIAL_BUFSIZE);
    memset(m_nmea_buf, 0, GPS_SERIAL_BUFSIZE);
    
    return true;
}

/**************************************
 * GpsBasic::service
 **************************************/
GpsBasic::NmeaType GpsBasic::service()
{
    NmeaType nmea = NMEA_NONE;
    if (getNmea())
    {
        nmea = parseSentence();
    }
    return nmea;
}

/**************************************
 * GpsBasic::copyNmea
 **************************************/
int GpsBasic::copyNmea(char* buf)
{
    strcpy(buf, m_nmea_buf);
    return strlen(m_nmea_buf);
}

/**************************************
 * GpsBasic::sendCmd
 **************************************/
int GpsBasic::sendCmd(const char* cmd)
{
    int bytes = 0;
    int len = strlen(cmd);
    if (m_pGpsSerial->availableForWrite() >= len)
    {
        bytes = m_pGpsSerial->write(cmd, len);
    }
    return bytes;
}

/******************************************************************************
 * Protected methods and functions.
 ******************************************************************************/

/**************************************
 * GpsBasic::checksumOk()
 **************************************/ 
bool GpsBasic::checksumOk()
{
    int index = 1;
    uint8_t sumCalc = 0;
    
    do
    {
        sumCalc ^= m_gps_rxbuf[index];
    } while (m_gps_rxbuf[++index] != '*');
    
    uint8_t sumNmea = (hex2bin(m_gps_rxbuf[index+1]) << 4) + hex2bin(m_gps_rxbuf[index+2]);
    return (sumNmea == sumCalc);
}

/**************************************
 * GpsBasic::getNmea()
 **************************************/
bool GpsBasic::getNmea()
{
    bool gotNmea = false;
    
    while (m_pGpsSerial->available())
    {
        char c = m_pGpsSerial->read();
        if (c == '$')
        {
            // Start of sentence.
            *m_gps_rxbuf = c;
            m_index = 1;
        }
        else if (c == '\x0A')
        {
            // <LF> found - end of sentence.
            m_gps_rxbuf[m_index] = 0;
            gotNmea = true;
            
            // Remove preceeding <CR>.
            if ((m_index > 0) && (m_gps_rxbuf[m_index-1] == '\x0D')) 
                m_gps_rxbuf[m_index-1] = 0;

            // Save a copy of the NMEA sentence prior to parsing.
            strcpy(m_nmea_buf, m_gps_rxbuf);
            break;
        }
        else
        {
            if (m_index < GPS_SERIAL_BUFSIZE-1)
            {
                // Append character to sentence.
                m_gps_rxbuf[m_index++] = c;
            }
            else
            {
                // Line wrap.  Sentence will not be parsed.
                *m_gps_rxbuf = c;
                m_index = 1;
            }
        }
    }
    
    return gotNmea;
}

/**************************************
 * GpsBasic::parseSentence()
 **************************************/
GpsBasic::NmeaType GpsBasic::parseSentence()
{
    NmeaType nmea = NMEA_NONE;
    
    /****
     * Make sure we have a complete NMEA sentence.
     * Minimum format is $PPMMM*CC00
     *   $ is the start of sentence character
     *   PP is the two-letter NMEA prefix (aka Talker ID)
     *   MMM is the three-letter (minimum) NMEA (or proprietary) message
     *   * is the end of sentence character
     *   CC is the two-letter ASCII checksum
     *   00 are two zeros that replace the CR/LF terminator
    ****/
    if ((m_index > 9) && (*m_gps_rxbuf == '$') && (m_gps_rxbuf[m_index-4] == '*'))
    {
        if(checksumOk())
        {
            // Convert the two-character NMEA-0183 prefix to an integer for easy comparison.
            uint16_t nmea_pfx = (uint16_t(m_gps_rxbuf[1]) << 8) + uint16_t(m_gps_rxbuf[2]);
            
            char* p_msg = &m_gps_rxbuf[3];  // Pointer to NMEA message
            
            nmea = NMEA_UNKNOWN;
            
            // Switch on the NMEA prefix, then do string compares on the NMEA message.
            // TODO: Find a more efficient (while still readable) way to do this.
            // Trying to reduce the number of string compares.
            // Thought about using a tree structure, but felt it was too unreadable.
            switch (nmea_pfx)
            {
                case 0x4741:   // GA - Galileo Positioning System
                break;
                
                case 0x4742:   // GB - BeiDou System (BDS)
                break;
                
                case 0x474C:   // GL - GLONASS receiver
                break;
                
                case 0x474E:   // GN - Global Navigation Satellite System (GNSS)
                break;
                
                case 0x4750:   // GP - Global Positioning System (GPS)
                {
                    if (strncmp("GGA", p_msg, 3) == 0)
                    {
                        parseGpgga();
                        nmea = NMEA_GPGGA;
                    }
                    else if (strncmp("GSA", p_msg, 3) == 0)
                    {
                        parseGpgsa();
                        nmea = NMEA_GPGSA;
                    }
                    else if (strncmp("GSV", p_msg, 3) == 0)
                    {
                        // We don't actually parse this message in the base class.
                        // There can be multiple messages in a sequence containing info
                        // for each of the satellites in view.
                        parseGpgsv();
                        nmea = NMEA_GPGSV;
                    }
                    else if (strncmp("RMC", p_msg, 3) == 0)
                    {
                        parseGprmc();
                        nmea = NMEA_GPRMC;
                    }
                    else if (strncmp("VTG", p_msg, 3) == 0)
                    {
                        parseGpvtg();
                        nmea = NMEA_GPVTG;
                    }
                }
                break;

                default:
                break;
            }
        }
    }
    
    return nmea;
}

/**************************************
 * GpsBasic::parseGpgga()
 **************************************/
void GpsBasic::parseGpgga()
{
    // Parse the GPGGA sentence.
    dummy_ptr      = nmeaStrtok(&m_gps_rxbuf[7]); // UTC time HHMMSS (not used)
    dummy_ptr      = nmeaStrtok(NULL);  // Latitude DDMM.mmm (not used)
    dummy_ptr      = nmeaStrtok(NULL);  // Hemisphere N/S (not used)
    dummy_ptr      = nmeaStrtok(NULL);  // Longitude DDDMM.mmm (not used)
    dummy_ptr      = nmeaStrtok(NULL);  // Hemisphere E/W (not used)
    char* qualStr  = nmeaStrtok(NULL);  // Fix quality
    char* satStr   = nmeaStrtok(NULL);  // Number of satellites in view
    char* hdopStr  = nmeaStrtok(NULL);  // Horizontal Dilution of Precision (HDOP)
    char* altStr   = nmeaStrtok(NULL);  // Altitude, meters above mean sea level
    char* altmStr  = nmeaStrtok(NULL);  // Altitude units (M)
    char* geoidStr = nmeaStrtok(NULL);  // Height of geoid above WGS84 ellipsoid, meters
    dummy_ptr      = nmeaStrtok(NULL);  // Geoid height units (M) (not used)
    
    m_data.qual    = *qualStr - '0';
    m_data.numsats = asc2dec(satStr);
    m_data.hdop    = atof(hdopStr);
    m_data.alt     = atof(altStr);
    m_data.alt_m   = *altmStr;
    m_data.geoid   = atof(geoidStr);
}

/**************************************
 * GpsBasic::parseGpgsa()
 **************************************/
void GpsBasic::parseGpgsa()
{
    // Parse the GPGSA sentence.
    char* m1Str    = nmeaStrtok(&m_gps_rxbuf[7]); // Mode 1: A/M
    char* m2Str    = nmeaStrtok(NULL);  // Mode 2: 1, 2, 3
    dummy_ptr      = nmeaStrtok(NULL);  // Satellite 1 (not used)
    dummy_ptr      = nmeaStrtok(NULL);  // Satellite 2 (not used)
    dummy_ptr      = nmeaStrtok(NULL);  // Satellite 3 (not used)
    dummy_ptr      = nmeaStrtok(NULL);  // Satellite 4 (not used)
    dummy_ptr      = nmeaStrtok(NULL);  // Satellite 5 (not used)
    dummy_ptr      = nmeaStrtok(NULL);  // Satellite 6 (not used)
    dummy_ptr      = nmeaStrtok(NULL);  // Satellite 7 (not used)
    dummy_ptr      = nmeaStrtok(NULL);  // Satellite 8 (not used)
    dummy_ptr      = nmeaStrtok(NULL);  // Satellite 9 (not used)
    dummy_ptr      = nmeaStrtok(NULL);  // Satellite 10 (not used)
    dummy_ptr      = nmeaStrtok(NULL);  // Satellite 11 (not used)
    dummy_ptr      = nmeaStrtok(NULL);  // Satellite 12 (not used)
    char* pdopStr  = nmeaStrtok(NULL);  // Position Dilution of Precision (PDOP)
    char* hdopStr  = nmeaStrtok(NULL);  // Horizontal Dilution of Precision (HDOP)
    char* vdopStr  = nmeaStrtok(NULL);  // Vertical Dilution of Precision (VDOP)
    
    m_data.mode1   = *m1Str;
    m_data.mode2   = *m2Str - '0';
    m_data.pdop    = atof(pdopStr);
    m_data.hdop    = atof(hdopStr);
    m_data.vdop    = atof(vdopStr);
}

/**************************************
 * GpsBasic::parseGprmc()
 **************************************/
void GpsBasic::parseGprmc()
{
    static const float MIN_FACTOR = 1.0/60.0;
    
    float temp;
    
    // Parse the GPRMC sentence.
    char* timeStr  = nmeaStrtok(&m_gps_rxbuf[7]); // UTC time HHMMSS.sss
    char* status   = nmeaStrtok(NULL);  // Fix status
    char* latStr   = nmeaStrtok(NULL);  // Latitude DDMM.mmm
    char* latH     = nmeaStrtok(NULL);  // Hemisphere N/S
    char* lonStr   = nmeaStrtok(NULL);  // Longitude DDDMM.mmm
    char* lonH     = nmeaStrtok(NULL);  // Hemisphere E/W
    char* speedStr = nmeaStrtok(NULL);  // Speed over ground in knots
    char* trackStr = nmeaStrtok(NULL);  // Track angle in degrees True    
    char* dateStr  = nmeaStrtok(NULL);  // UTC date DDMMYY
    char* magStr   = nmeaStrtok(NULL);  // Magnetic variation DDD.d
    char* magDir   = nmeaStrtok(NULL);  // Magnetic variation E/W
    
    if (*status == 'A')
    {
        m_data.fix = true;
        
        // Time.
        m_data.time_r = atof(timeStr);
        m_data.hour   = asc2dec(&timeStr[0]);
        m_data.minute = asc2dec(&timeStr[2]);
        m_data.second = asc2dec(&timeStr[4]);
        
        // Date.
        m_data.date_r = (uint32_t)atol(dateStr);
        m_data.day    = asc2dec(&dateStr[0]);
        m_data.month  = asc2dec(&dateStr[2]);
        m_data.year   = asc2dec(&dateStr[4]) + 2000;  // Offset from 2000
        
        // Latitude.
        m_data.lat_r = atof(latStr);
        m_data.lat_h = *latH;
        temp = (float)asc2dec(&latStr[0]);
        temp += (float)asc2dec(&latStr[2]) * MIN_FACTOR;
        temp += atof(&latStr[4]) * MIN_FACTOR;
        if (*latH == 'S') temp = -temp;
        m_data.lat_d = temp;
        
        // Longitude.
        m_data.lon_r = atof(lonStr);
        m_data.lon_h = *lonH;
        temp = (float)asc3dec(&lonStr[0]);
        temp += (float)asc2dec(&lonStr[3]) * MIN_FACTOR;
        temp += atof(&lonStr[5]) * MIN_FACTOR;
        if (*lonH == 'W') temp = -temp;
        m_data.lon_d = temp;
        
        // Speed over ground.
        m_data.speed_n = atof(speedStr);
        
        // Track angle.
        m_data.track = atof(trackStr);
        
        // Magnetic variation.
        temp = atof(magStr);
        if (*magDir == 'W') temp = -temp;
        m_data.var = temp;
    }
    else
    {
        m_data.fix = false;
    }
}

/**************************************
 * GpsBasic::parseGpvtg()
 **************************************/
void GpsBasic::parseGpvtg()
{
    // Parse the GPVTG sentence.
    char* tcStr    = nmeaStrtok(&m_gps_rxbuf[7]); // Course in degrees (true)
    dummy_ptr      = nmeaStrtok(NULL);  // T = True heading (not used)
    dummy_ptr      = nmeaStrtok(NULL);  // Course in degrees (magnetic) (not used)
    dummy_ptr      = nmeaStrtok(NULL);  // M = Magnetic heading (not used)
    char* spdnPtr  = nmeaStrtok(NULL);  // Horizontal speed in knots
    dummy_ptr      = nmeaStrtok(NULL);  // N = Knots indicator (not used)
    char* spdkPtr  = nmeaStrtok(NULL);  // Horizontal speed in km/h
    dummy_ptr      = nmeaStrtok(NULL);  // K = km/h indicator (not used)
    
    m_data.tcourse = atof(tcStr);
    m_data.speed_n = atof(spdnPtr);
    m_data.speed_k = atof(spdkPtr);
}

/**************************************
 * GpsBasic::nmeaStrtok()
 **************************************/
char* GpsBasic::nmeaStrtok(char* pStr)
{
    /****
     * Note: this works differently from standard strok() out of necessity.
     * strtok() determines the beginning of a token by scanning from the starting
     * point until it finds the first non-delimiter.  This causes errors when 
     * parsing empty fields in NMEA sentences.  nmeaStrtok() always uses the 
     * current position as the starting point.  If it immediately encounters 
     * a delimeter, it returns an empty string and advances to the next starting 
     * point.
     * Example: parsing "12,40,,,1.03" returns "12", "40", "", "", "1.03"
     * strtok() would would ignore empty strings and return "12", "40", "1.03"
     ****/
    
    if (pStr) m_pStrtok = pStr;  // New string to tokenize
    
    char* p = m_pStrtok;         // Search pointer
    char* pToken = m_pStrtok;    // Token always uses current starting point
    
    while (*p != 0)
    {
        char c = *p;
        if (c == ',')
        {
            // Found delimiter. Replace with string terminator.
            // Advance starting point to next location.
            *p = 0;
            m_pStrtok = p + 1;
        }
        else if (c == '*')
        {
            // Found end of sentence. Replace with string terminator.
            // Keep starting point on the empty string.
            *p = 0;
            m_pStrtok = p;
        }
        else
        {
            // Found non-delimiter character. Advance search pointer.
            p++;
        }
    }
    return pToken;  // Return pointer to current token
}


/*****************************************************************************
 * Private methods and functions.
 ******************************************************************************/

/**************************************
 * GpsBasic::asc2dec()
 **************************************/
static uint16_t asc2dec(char* asc)
{
    int dec = ((asc[0] - '0') * 10) + (asc[1] - '0');
    return dec;
}

/**************************************
 * GpsBasic::asc3dec
 **************************************/
static uint16_t asc3dec(char* asc)
{
    int dec = ((asc[0] - '0') * 100) + ((asc[1] - '0') * 10) + (asc[2] - '0');
    return dec;
}

/**************************************
 * GpsBasic::hex2bin()
 **************************************/
static uint8_t hex2bin(char hex)
{
    uint8_t bin = 0;
    if ((hex >= '0') && (hex <= '9')) bin = hex - '0';
    else if ((hex >= 'A') && (hex <= 'F')) bin = hex - 'A' + 10;
    else if ((hex >= 'a') && (hex <= 'f')) bin = hex - 'a' + 10;
    return bin;
}

// End of file.
