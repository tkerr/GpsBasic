/******************************************************************************
 * GpsData.cpp
 * Copyright (c) 2022 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * GPS data container class.
 *
 * Contains parsed and formatted GPS data.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "GpsData.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/


/******************************************************************************
 * Local data.
 ******************************************************************************/


/******************************************************************************
 * Public functions.
 ******************************************************************************/

/**************************************
 * GpsData::GpsData
 **************************************/
GpsData::GpsData()
{
    clear();
}

/**************************************
 * GpsData::clear()
 **************************************/  
void GpsData::clear()
{
    fix     = false;
    time_r  = 0.0;
    lat_r   = 0.0;
    lat_d   = 0.0;
    lon_r   = 0.0;
    lon_d   = 0.0;
    alt     = 0.0;
    geoid   = 0.0;
    tcourse = 0.0;
    speed_n = 0.0;
    speed_k = 0.0;
    track   = 0.0;
    var     = 0.0;
    pdop    = 0.0;
    hdop    = 0.0;
    vdop    = 0.0;
    lat_h   = ' ';
    lon_h   = ' ';
    alt_m   = ' ';
    mode1   = ' ';
    date_r  = 0;
    mode2   = 0;
    numsats = 0;
    year    = 0;
    month   = 0;
    day     = 0;
    hour    = 0;
    minute  = 0;
    second  = 0;
    millis  = 0;
    qual    = 0;
}


/**************************************
 * GpsSatView::GpsSatView
 **************************************/
GpsSatView::GpsSatView()
{
    clear();
}


/**************************************
 * GpsSatView::clear()
 **************************************/  
void GpsSatView::clear()
{
    prn = 0;
    el_deg = -1;
    az_deg = -1;
    snr = -1;
}


/**************************************
 * GpgsvData::GpgsvData
 **************************************/
GpgsvData::GpgsvData()
{
    clear();
}


/**************************************
 * GpgsvData::clear()
 **************************************/  
void GpgsvData::clear()
{
    sats_in_view = 0;
    num_msgs = 0;
    last_msg = 0;
    for (int i = 0; i < GpgsvData::MAX_SATS; i++)
    {
        sat[i].clear();
    }
}


/*****************************************************************************
 * Private functions.
 ******************************************************************************/

// End of file.
