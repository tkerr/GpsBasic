/*****************************************************************************
 * GpsData.h
 * Copyright (c) 2022 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ****************************************************************************/

/**
 * @file
 * @brief
 * GPS data container class.
 *
 * Contains parsed and formatted GPS data.
 */
#ifndef _GPS_DATA_H_
#define _GPS_DATA_H_

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stdbool.h>
#include <stdint.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/


/******************************************************************************
 * Public definitions.
 ******************************************************************************/


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

 
/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/

/**
 * @class GpsData
 * @brief GPS data container class.
 *
 * Contains parsed and formatted GPS data.
 */
class GpsData
{
public:

    float    time_r;  //!< UTC time in raw format: hhmmss.sss
    float    lat_d;   //!< Latitude in decimal degrees, positive = N, negative = S
    float    lat_r;   //!< Latitude in raw format: ddmm.mmmm
    float    lon_d;   //!< Longitude in decimal degrees, positive = E, negative = W
    float    lon_r;   //!< Longitude in raw format: dddmm.mmmm
    float    alt;     //!< Altitude in meters above mean sea level
    float    geoid;   //!< Height above geoid above WGS84 ellipsoid in meters
    float    tcourse; //!< Course in degrees, true heading
    float    speed_n; //!< Speed over ground in knots
    float    speed_k; //!< Speed over ground in km/h
    float    track;   //!< Track angle in degrees, True
    float    var;     //!< Magnetic variation
    float    pdop;    //!< Position Dilution of Precision (PDOP)
    float    hdop;    //!< Horizontal Dilution of Precision (HDOP)
    float    vdop;    //!< Vertical Dilution of Precision (VDOP)
    char     lat_h;   //!< Latitude hemisphere N/S
    char     lon_h;   //!< Longitude hemisphere E/W
    char     alt_m;   //!< Altitude units M
    char     mode1;   //!< Mode: A = Auto 2D/3D, M = Forced 2D/3D
    uint32_t date_r;  //!< UTC date in raw format: DDMMYY
    uint16_t mode2;   //!< Mode: 1 = No fix, 2 = 2D, 3 = 3D
    uint16_t numsats; //!< Number of satellites in view
    uint16_t year;    //!< UTC time year component
    uint16_t month;   //!< UTC time month component
    uint16_t day;     //!< UTC time day of month component
    uint16_t hour;    //!< UTC time hours component
    uint16_t minute;  //!< UTC time minutes component
    uint16_t second;  //!< UTC time seconds component
    uint16_t millis;  //!< UTC time milliseconds component
    uint16_t qual;    //!< GPS fix quality code (0 = Invalid, 1 = GPS fix, 2 = DGPS fix) 
    bool     fix;     //!< GPS fix flag; data is valid if true, invalid if false
    
    GpsData();     //!< Default constructor
    void clear();  //!< Clear all data
};


/**
 * @class GpsSatView
 * @brief GPS satellite data container class.
 *
 * Contains parsed and formatted GPS satellite data typically obtained
 * from the $GPGSV NMEA sentence.
 */
class GpsSatView
{
public:
    int prn;      //!< Satellite PRN number (0 if not present)
    int el_deg;   //!< Elevation in degrees (0-90) or -1 if not present
    int az_deg;   //!< Azimuth, degrees from true north (0-359) or -1 if not present
    int snr;      //!< SNR 0-99 dB (-1 if not tracking)
    
    GpsSatView();  //!< Default constructor
    void clear();  //!< Clear all data
};


/**
 * @class GpgsvData
 * @brief GPS satellites in view data container class.
 *
 * Contains parsed and formatted GPS satellite data from the $GPGSV 
 * NMEA sentence.
 */
class GpgsvData
{
public:
    static const int MAX_SATS = 12;  //!< Maximum number of satellites in data structure
    int sats_in_view;                //!< Number of satellites in view
    int num_msgs;                    //!< Total number of GPGSV messages
    int last_msg;                    //!< Last GPGSV message number parsed
    GpsSatView sat[MAX_SATS];        //!< Array of satellites in view

    GpgsvData();   //!< Default constructor
    void clear();  //!< Clear all data
};

#endif // _GPS_DATA_H_
