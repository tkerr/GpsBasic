/*****************************************************************************
 * GpsBasic.h
 * Copyright (c) 2022 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ****************************************************************************/

/**
 * @file
 * @brief
 * Arduino basic GPS controller class.
 *
 * Provides basic hardware-independent GPS functionality for receiving and 
 * parsing NMEA messages.  Can be subclassed to support specific hardware 
 * modules.
 */
#ifndef _GPS_BASIC_H_
#define _GPS_BASIC_H_

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stdbool.h>



/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "GpsData.h"
#include "ISerial.h"


/******************************************************************************
 * Public definitions.
 ******************************************************************************/
 
 
/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

 
/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/
 
/**
 * @class GpsBasic
 * @brief Arduino basic GPS controller class.
 *
 * Provides basic hardware-independent GPS functionality for receiving and 
 * parsing NMEA messages.  Can be subclassed to support specific hardware 
 * modules.
 * 
 */
class GpsBasic
{
public:

    /**
     * @brief The size of the GPS serial receive buffer in bytes.
     */
    static const int GPS_SERIAL_BUFSIZE = 128;
    
    /**
     * @brief Some common NMEA sentence types.
     *
     * See NMEA 0183 V4.11 Sentence Descriptions for a complete list of 
     * approved sentence formatters.
     */
    enum NmeaType
    {
        NMEA_NONE = 0,  //!< None received (not part of NMEA 0183)
        NMEA_GPGGA,     //!< Global Positioning System Fix Data
        NMEA_GPGLL,     //!< Geographic Position - Latitude/Longitude
        NMEA_GPGSA,     //!< GNSS DOP and Active Satellites
        NMEA_GPGSV,     //!< GNSS Satellites in view
        NEMA_GPMSS,     //!< MSK Receiver Signal
        NMEA_GPRMC,     //!< Recommended Minimum Specific GNSS Data
        NMEA_GPSTN,     //!< Multiple Data ID
        NMEA_GPVTG,     //!< Course over ground and speed
        NMEA_GPXTE,     //!< Cross-Track Error, Measured
        NMEA_GPZDA,     //!< Time and Date
        NMEA_UNKNOWN = 999, //!< Unknown/unsupported (not part of NMEA 0183)
    };
    
    /**
     * @brief Default constructor.
     */
    GpsBasic();
    
    /**
     * @brief Set the serial interface to be used by the GPS hardware.
     *
     * Note that the serial interface must be initialized separately.
     *
     * @param pSerial Pointer to the serial interface.
     */
    inline void setGpsSerial(ISerial* pSerial) {m_pGpsSerial = pSerial;}
    
    /**
     * @brief Initialize the basic GPS controller object.
     *
     * @return True if initialization successful, false otherwise.
     */
    virtual bool begin();
    
    /**
     * @brief Periodic service routine.
     *
     * Call at regular intervals to service the GPS hardware and provide
     * updates.
     *
     * @return The NMEA sentence type that was just parsed, or NMEA_NONE if none was parsed.
     */
    virtual NmeaType service();
    
    /**
     * @brief Return true if GPS has a fix, false otherwise.
     */
    inline bool hasFix() {return m_data.fix;}
    
    /**
     * @brief Return the parsed and formatted GPS data.
     */
    inline const GpsData& data() {return m_data;}
    
    /**
     * @brief Get a copy of the last received NMEA sentence.
     *
     * @param buf Pointer to a character buffer to receive the NMEA sentence as a C-string.
     * NOTE: The sentence does not contain the CR/LF terminator.
     *
     * @return The number of bytes in the sentence.
     */
    int copyNmea(char* buf);
    
    /**
     * @brief Send a command to the GPS device.
     *
     * If the GPS device provides a response, it can be found by calling service()
     * until it responds with NmeaType::NMEA_UNKNOWN and then calling copyNmea()
     * to get the response string.
     *
     * @param cmd The command to send.  Must be a complete and properly formatted  
     * NMEA sentence including the initial $, the checksum and CR/LF terminator.
     *
     * @return The number of bytes sent to the GPS device.
     */
    int sendCmd(const char* cmd);

protected:

    /**
     * @brief Confirm the NMEA sentence checksum.
     * @return True if checksum is correct, false otherwise.
     */
    bool checksumOk();
    
    /**
     * @brief Receive a NMEA sentence from the GPS device.
     * 
     * Call at regular intervals via the service() method to receive a complete sentence.
     * A NMEA sentence starts with '$' and ends with '\r\n'.
     *
     * The NMEA sentence can be retrieved by calling copyNmea().
     * 
     * @return True when a complete NMEA sentence is received, false otherwise.
     */
    bool getNmea();
    
    /**
     * @brief Parse a NMEA sentence.
     *
     * State machine used to parse NMEA sentences.
     * Call at regular intervals via the service() method to parse complete sentences.
     *
     * @return The NMEA sentence type that was just parsed, or NMEA_NONE if none was parsed.
     */
    virtual NmeaType parseSentence();
    
    virtual void parseGpgga();  //!< Parse the $GPGGA NMEA sentence
    virtual void parseGpgsa();  //!< Parse the $GPGSA NMEA sentence
    virtual void parseGprmc();  //!< Parse the $GPRMC NMEA sentence
    virtual void parseGpvtg();  //!< Parse the $GPVTG NMEA sentence
    
    virtual void parseGpgsv() {return;} //!< Parse the $GPVTG NMEA sentence - base class does nothing
    
    char*    nmeaStrtok(char* pStr);  //!< Special version of strtok() to parse NMEA sentence

    char     m_gps_rxbuf[GPS_SERIAL_BUFSIZE];  //!< GPS NMEA data buffer
    char     m_nmea_buf[GPS_SERIAL_BUFSIZE];   //!< NMEA sentence buffer saved for user
    char*    m_pStrtok;     //!< Pointer used by nmeaStrtok()
    uint8_t  m_index;       //!< Index into GPS data buffer
    GpsData  m_data;        //!< GPS data container
    ISerial* m_pGpsSerial;  //!< Pointer to serial interface used by the GPS hardware

private:

};


#endif // _GPS_BASIC_H_
