# GpsBasic #
Arduino basic GPS controller class.

Provides basic hardware-independent GPS functionality for receiving and parsing NMEA messages.  Can be subclassed to support specific hardware modules.

### Dependencies ###
ISerial library: https://gitlab.com/tkerr/ISerial

### Author ###
Tom Kerr AB3GY

### License ###
Released under the MIT License  
https://opensource.org/licenses/MIT
